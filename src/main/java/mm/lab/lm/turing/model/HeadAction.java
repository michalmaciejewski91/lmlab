package mm.lab.lm.turing.model;

public class HeadAction {

	private Character writeData;
	private DirectionEnum moveDirection;
	private TuringState nextState;

	public HeadAction(Character writeData, DirectionEnum moveDirection, TuringState nextState) {
		this.writeData = writeData;
		this.moveDirection = moveDirection;
		this.nextState = nextState;
	}

	public Character getWriteData() {
		return writeData;
	}

	public DirectionEnum getMoveDirection() {
		return moveDirection;
	}

	public TuringState getNextState() {
		return nextState;
	}
}
