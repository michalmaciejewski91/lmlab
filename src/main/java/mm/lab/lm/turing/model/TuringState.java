package mm.lab.lm.turing.model;

import java.util.HashMap;
import java.util.Map;

public class TuringState {

	public static final String CURRENT_STATE_COLOR = "GREEN";

	private Map<Character, HeadAction> transferMap;
	private boolean finalState;

	private String name;

	private TuringState() {
		this.transferMap = new HashMap<>();
	}

	public TuringState(int id, boolean finalState) {
		this();
		this.finalState = finalState;
		this.name = "Q" + id;
	}

	public Map<Character, HeadAction> getTransferMap() {
		return transferMap;
	}

	public boolean isFinalState() {
		return finalState;
	}

	public String getName() {
		return name;
	}
}
