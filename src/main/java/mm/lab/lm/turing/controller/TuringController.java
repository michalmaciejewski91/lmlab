package mm.lab.lm.turing.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import mm.lab.lm.turing.model.DirectionEnum;
import mm.lab.lm.turing.model.HeadAction;
import mm.lab.lm.turing.model.TuringState;
import org.apache.log4j.Logger;

public class TuringController {

	private static final Logger LOGGER = Logger.getLogger(TuringController.class.getName());

	@FXML
	private TextField inputTextField;

	@FXML
	private TextField historyTextField;

	@FXML
	private Button nextStepButton;

	@FXML
	private Label wrongInputLabel;

	@FXML
	private TableView<ObservableList> tapeTableView;

	private List<TuringState> states = initData();
	private TuringState currentState;
	private int inputTextIterator = 0;
	private String tapeText;

	private boolean completed = false;

	@FXML
	private void runButtonHandler() {
		if (init()) {
			while (!completed) {
				nextStep();
			}
		}
	}

	@FXML
	private void runStepByStepButtonHandler() {
		if (init()) {
			show(nextStepButton);
		}
	}

	private boolean init() {
		cleanPrevValidationEffects();
		if (isInputOK()) {
			tapeText = inputTextField.getText();
			currentState = states.get(0);
			historyTextField.setText(currentState.getName());
			createTapeTable();
			return true;
		} else {
			LOGGER.debug("wrong input");
			show(wrongInputLabel);
			return false;
		}
	}

	private void createTapeTable() {
		Pane header = (Pane) tapeTableView.lookup("TableHeaderRow");
		header.setMinHeight(0);
		header.setPrefHeight(0);
		header.setMaxHeight(0);
		hide(header);

		tapeTableView.getColumns().clear();
		for (int i = 0; i < tapeText.length(); i++) {
			final int j = i;
			TableColumn col = new TableColumn();
			col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList, String>, ObservableValue<String>>() {
				public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
					return new SimpleStringProperty(param.getValue().get(j).toString());
				}
			});
			if (i == inputTextIterator) {
				markColumnAsActive(col);
			}
			tapeTableView.getColumns().add(col);
		}

		ObservableList<ObservableList> data = FXCollections.observableArrayList();
		ObservableList<Character> row = FXCollections.observableArrayList();
		for (int i = 0; i < tapeText.length(); i++) {
			row.add(tapeText.charAt(i));
		}
		data.add(row);
		tapeTableView.setItems(data);

		show(tapeTableView);
	}

	private void markColumnAsActive(TableColumn col) {
		col.setStyle("-fx-background-color: " + TuringState.CURRENT_STATE_COLOR + ";");
	}

	private boolean isInputOK() {
		return inputTextField.getText().matches("[E]*[01]+#[01]+[E]*");
	}

	private void cleanPrevValidationEffects() {
		hide(wrongInputLabel);
		hide(nextStepButton);
		inputTextIterator = 0;
		historyTextField.clear();
		tapeTableView.getColumns().clear();
		completed = false;
	}

	@FXML
	private void nextStep() {
		try {
			moveOneStep();
			updateGUI();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		completed = currentState.isFinalState();
		if (completed) {
			hide(nextStepButton);
		}
	}

	private void moveOneStep() throws Exception {
		Character currentChar = tapeText.charAt(inputTextIterator);
		Map<Character, HeadAction> transferMap = currentState.getTransferMap();
		if (transferMap.containsKey(currentChar)) {
			HeadAction headAction = transferMap.get(currentChar);
			write(headAction.getWriteData());
			moveHead(headAction);
			currentState = headAction.getNextState();
			if (inputTextIterator < 0) {
				tapeText = "E" + tapeText;
				inputTextIterator = 0;
				createTapeTable();
			} else if (inputTextIterator == tapeText.length()) {
				tapeText += "E";
				createTapeTable();
			}
		} else {
			throw new Exception("Something went wrong. Check logs.");
		}
	}

	private void write(Character writeData) {
		if (writeData != null) {
			StringBuilder tapeTextBuilder = new StringBuilder(tapeText);
			tapeTextBuilder.setCharAt(inputTextIterator, writeData);
			tapeText = tapeTextBuilder.toString();
		}
	}

	private void moveHead(HeadAction headAction) {
		DirectionEnum moveDirection = headAction.getMoveDirection();
		if (moveDirection != null) {
			if (moveDirection.equals(DirectionEnum.RIGHT)) {
				inputTextIterator++;
			} else if (moveDirection.equals(DirectionEnum.LEFT)) {
				inputTextIterator--;
			}
		}
	}

	private void updateHistory() {
		historyTextField.setText(historyTextField.getText() + " -> " + currentState.getName());
		historyTextField.positionCaret(historyTextField.getText().length());
	}

	private void updateGUI() {
		createTapeTable();
		updateHistory();
	}

	private static List<TuringState> initData() {
		List<TuringState> states = new ArrayList<>();
		states.add(new TuringState(0, false));
		states.add(new TuringState(1, false));
		states.add(new TuringState(2, false));
		states.add(new TuringState(3, false));
		states.add(new TuringState(4, false));
		states.add(new TuringState(5, false));
		states.add(new TuringState(6, true));

		Map<Character, HeadAction> q0TransferMap = states.get(0).getTransferMap();
		q0TransferMap.put('1', new HeadAction(null, DirectionEnum.RIGHT, states.get(1)));
		q0TransferMap.put('0', new HeadAction(null, DirectionEnum.RIGHT, states.get(1)));
		q0TransferMap.put('E', new HeadAction(null, DirectionEnum.RIGHT, states.get(0)));

		Map<Character, HeadAction> q1TransferMap = states.get(1).getTransferMap();
		q1TransferMap.put('1', new HeadAction(null, DirectionEnum.RIGHT, states.get(1)));
		q1TransferMap.put('0', new HeadAction(null, DirectionEnum.RIGHT, states.get(1)));
		q1TransferMap.put('#', new HeadAction(null, DirectionEnum.RIGHT, states.get(1)));
		q1TransferMap.put('E', new HeadAction(null, DirectionEnum.LEFT, states.get(2)));

		Map<Character, HeadAction> q2TransferMap = states.get(2).getTransferMap();
		q2TransferMap.put('1', new HeadAction('0', DirectionEnum.LEFT, states.get(3)));
		q2TransferMap.put('0', new HeadAction('1', DirectionEnum.LEFT, states.get(2)));
		q2TransferMap.put('#', new HeadAction(null, DirectionEnum.RIGHT, states.get(5)));

		Map<Character, HeadAction> q3TransferMap = states.get(3).getTransferMap();
		q3TransferMap.put('1', new HeadAction(null, DirectionEnum.LEFT, states.get(3)));
		q3TransferMap.put('0', new HeadAction(null, DirectionEnum.LEFT, states.get(3)));
		q3TransferMap.put('#', new HeadAction(null, DirectionEnum.LEFT, states.get(4)));
		q3TransferMap.put('E', new HeadAction(null, DirectionEnum.LEFT, states.get(3)));

		Map<Character, HeadAction> q4TransferMap = states.get(4).getTransferMap();
		q4TransferMap.put('1', new HeadAction('0', DirectionEnum.LEFT, states.get(4)));
		q4TransferMap.put('0', new HeadAction('1', DirectionEnum.RIGHT, states.get(1)));
		q4TransferMap.put('E', new HeadAction('1', DirectionEnum.RIGHT, states.get(1)));

		Map<Character, HeadAction> q5TransferMap = states.get(5).getTransferMap();
		q5TransferMap.put('1', new HeadAction('0', DirectionEnum.RIGHT, states.get(5)));
		q5TransferMap.put('E', new HeadAction(null, null, states.get(6)));

		return states;
	}

	private void hide(Node nodeToHide) {
		nodeToHide.setVisible(false);
	}

	private void show(Node nodeToShow) {
		nodeToShow.setVisible(true);
	}
}
