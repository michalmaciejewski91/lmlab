package mm.lab.lm;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LMRunner extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hhmmss");
		System.setProperty("current.date", dateFormat.format(new Date()));

		Parent root = FXMLLoader.load(getClass().getResource("/view/main.fxml"));
		primaryStage.setTitle("LMLab - project created for university purpose");
		primaryStage.setScene(new Scene(root, 1100, 750));
		primaryStage.setResizable(false);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
