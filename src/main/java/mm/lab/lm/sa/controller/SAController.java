package mm.lab.lm.sa.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import org.apache.log4j.Logger;

public class SAController {

	private static final Logger LOGGER = Logger.getLogger(SAController.class.getName());

	@FXML
	private AnchorPane mainAnchorPane;

	@FXML
	private TextField inputTextField;

	@FXML
	private Label resultLabel;

	private static final String RESULT_LABEL_PREFIX = "Result: ";
	private static final String RESULT_SUCCESS_MSG = "Grammar accepted";
	private static final String RESULT_FAILURE_MSG = "Grammar not accepted";
	private static final String INPUT_SEPARATOR = "#";

	private List<Character> sentence = new ArrayList<>();
	private int charIndex = -1;

	@FXML
	private void analyzeButtonHandler() {
		boolean result = analyze(inputTextField.getText().split(INPUT_SEPARATOR));
		if (result) {
			setResult(RESULT_SUCCESS_MSG);
		} else {
			setResult(RESULT_FAILURE_MSG);
		}
	}

	private boolean analyze(String[] syntaxTable) {
		for (String syntax : syntaxTable) {
			if (!analyze(syntax)) {
				return false;
			}
		}
		return true;
	}

	private boolean analyze(String syntax) {
		sentence = getConvertedInput(syntax);
		return checkS();
	}

	private List<Character> getConvertedInput(String syntax) {
		List<Character> result = new ArrayList<>();
		for (int i = 0; i < syntax.length(); i++) {
			result.add(syntax.charAt(i));
		}
		result.add(';');
		return result;
	}

	@FXML
	private void loadFromFileButtonHandler() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select data file");
		fileChooser.setInitialDirectory(new File("./src/main/resources"));
		File loadedFile = fileChooser.showOpenDialog(mainAnchorPane.getScene().getWindow());
		if (loadedFile != null) {
			loadFromFile(loadedFile);
		}
	}

	private void loadFromFile(File file) {
		try {
			StringBuilder newInputText = new StringBuilder();
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				newInputText.append(scanner.nextLine());
				newInputText.append(INPUT_SEPARATOR);
			}
			inputTextField.setText(newInputText.substring(0, newInputText.lastIndexOf(INPUT_SEPARATOR)));
		} catch (FileNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private void setResult(String result) {
		resultLabel.setText(RESULT_LABEL_PREFIX + result);
	}

	private boolean checkS() {
		charIndex = -1;

		return checkW()
			&& checkT(';')
			&& checkZ()
			&& charIndex == sentence.size() - 1;
	}

	private boolean checkZ() {
		int prevCharIndex = charIndex;
		if (checkW()) {
			return checkT(';')
				&& checkZ();
		} else {
			charIndex = prevCharIndex;
			return true;
		}
	}

	private boolean checkW() {
		return checkP()
			&& checkWP();
	}

	private boolean checkWP() {
		int prevCharIndex = charIndex;
		if (checkO()) {
			return checkW();
		} else {
			charIndex = prevCharIndex;
			return true;
		}
	}

	private boolean checkP() {
		int prevCharIndex = charIndex;
		if (checkR()) {
			return true;
		} else {
			charIndex = prevCharIndex;
			return checkT('(')
				&& checkW()
				&& checkT(')');
		}
	}

	private boolean checkR() {
		return checkL()
			&& checkRp();
	}

	private boolean checkRp() {
		int prevCharIndex = charIndex;
		if (checkT('.')) {
			return checkL();
		} else {
			charIndex = prevCharIndex;
			return true;
		}
	}

	private boolean checkL() {
		int prevCharIndex = charIndex;
		if (checkC() && checkLp()) {
			return true;
		} else if (checkCP()) {
			return true;
		} else {
			charIndex = prevCharIndex;
			return checkC()
				&& checkCP()
				&& checkLp();
		}
	}

	private boolean checkLp() {
		int prevCharIndex = charIndex;
		if (checkL()) {
			return true;
		} else {
			charIndex = prevCharIndex;
			return true;
		}
	}

	private boolean checkT(char ch) {
		charIndex++;
		return charIndex < sentence.size()
			&& sentence.get(charIndex) == ch;
	}

	private boolean checkC() {
		charIndex++;
		return charIndex < sentence.size()
			&& "123456789".indexOf(sentence.get(charIndex)) != -1;
	}

	private boolean checkCP() {
		charIndex++;
		return charIndex < sentence.size()
			&& "0".indexOf(sentence.get(charIndex)) != -1;
	}

	private boolean checkO() {
		charIndex++;
		return charIndex < sentence.size()
			&& "*:+-/^".indexOf(sentence.get(charIndex)) != -1;
	}
}