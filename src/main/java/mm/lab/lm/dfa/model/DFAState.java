package mm.lab.lm.dfa.model;

import java.util.HashMap;
import java.util.Map;

public class DFAState {

	public static final String VALID_STATE_COLOR = "GREEN";
	public static final String INVALID_STATE_COLOR = "RED";
	public static final String INITIAL_STATE_COLOR = "WHITE";

	private String id;
	private boolean valid;
	private Map<Character, DFAState> transferMap;

	private String color;

	private DFAState() {
		this.transferMap = new HashMap<>();
		this.color = INITIAL_STATE_COLOR;

	}

	public DFAState(String id, boolean valid) {
		this();
		this.id = id;
		this.valid = valid;
		if (valid) {
			this.color = VALID_STATE_COLOR;
		} else {
			this.color = INVALID_STATE_COLOR;
		}
	}

	public String getId() {
		return id;
	}

	private boolean isValid() {
		return valid;
	}

	public Map<Character, DFAState> getTransferMap() {
		return transferMap;
	}

	public String getColor() {
		return color;
	}

	public String isValidAsString() {
		if (isValid()) {
			return "ACCEPTED";
		} else {
			return "NOT ACCEPTED";
		}
	}
}
