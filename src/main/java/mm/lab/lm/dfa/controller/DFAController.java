package mm.lab.lm.dfa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import mm.lab.lm.dfa.model.DFAState;
import org.apache.log4j.Logger;

public class DFAController {

	private static final Logger LOGGER = Logger.getLogger(DFAController.class.getName());

	@FXML
	private TextField inputTextField;

	@FXML
	private TextField historyTextField;

	@FXML
	private Button nextStepButton;

	@FXML
	private Circle stateQ0Circle;

	@FXML
	private Circle stateQ1Circle;

	@FXML
	private Circle stateQ2Circle;

	@FXML
	private Circle stateQ3Circle;

	@FXML
	private Label wrongInputLabel;

	private List<DFAState> states = initData();
	private DFAState currentState;
	private String inputText;
	private int inputTextIterator = 0;

	@FXML
	private void initValidation() {
		cleanPrevValidationEffects();
		inputText = inputTextField.getText();
		if (isInputOK()) {
			currentState = states.get(0);
			stateQ0Circle.setFill(Paint.valueOf(currentState.getColor()));
			historyTextField.setText(currentState.getId());
			nextStepButton.setVisible(true);
		} else {
			LOGGER.debug("Input chars doesn't belong to the alphabet");
			wrongInputLabel.setVisible(true);
		}
	}

	private boolean isInputOK() {
		return inputText.matches("[ab]+");
	}

	private void cleanPrevValidationEffects() {
		stateQ0Circle.setFill(Paint.valueOf(DFAState.INITIAL_STATE_COLOR));
		stateQ1Circle.setFill(Paint.valueOf(DFAState.INITIAL_STATE_COLOR));
		stateQ2Circle.setFill(Paint.valueOf(DFAState.INITIAL_STATE_COLOR));
		stateQ3Circle.setFill(Paint.valueOf(DFAState.INITIAL_STATE_COLOR));
		wrongInputLabel.setVisible(false);
		nextStepButton.setVisible(false);
		inputTextIterator = 0;
		historyTextField.clear();
	}

	@FXML
	private void nextStep() {
		DFAState prevState = currentState;
		char nextChar = inputText.charAt(inputTextIterator);
		currentState = prevState.getTransferMap().get(nextChar);
		try {
			updateGUI(prevState, currentState);
			updateHistory();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		if (inputTextIterator == inputText.length()) {
			nextStepButton.setVisible(false);
			LOGGER.info("Final state has been reached for '" + inputTextField.getText() + "'. " + currentState.isValidAsString());
			LOGGER.info("Transition history: " + historyTextField.getText());
		}
	}

	private void updateHistory() {
		String prevText = historyTextField.getText();
		historyTextField.setText(prevText + " -> " + currentState.getId());
		historyTextField.positionCaret(historyTextField.getText().length());
	}

	private void updateGUI(DFAState prevState, DFAState currentState) throws Exception {
		Circle prevStateCircle = getCircleBasedOnState(prevState);
		prevStateCircle.setFill(Paint.valueOf(DFAState.INITIAL_STATE_COLOR));

		Circle currentStateCircle = getCircleBasedOnState(currentState);
		currentStateCircle.setFill(Paint.valueOf(currentState.getColor()));

		inputTextIterator++;
	}

	private Circle getCircleBasedOnState(DFAState state) throws Exception {
		String stateId = state.getId();
		if ("Q0".equals(stateId)) {
			return stateQ0Circle;
		} else if ("Q1".equals(stateId)) {
			return stateQ1Circle;
		} else if ("Q2".equals(stateId)) {
			return stateQ2Circle;
		} else if ("Q3".equals(stateId)) {
			return stateQ3Circle;
		} else {
			throw new Exception("No Circle defined for state '" + stateId + "'.");
		}
	}

	private static List<DFAState> initData() {
		List<DFAState> states = new ArrayList<>();

		DFAState q0 = new DFAState("Q0", false);
		DFAState q1 = new DFAState("Q1", false);
		DFAState q2 = new DFAState("Q2", true);
		DFAState q3 = new DFAState("Q3", true);

		Map<Character, DFAState> q0TransferMap = q0.getTransferMap();
		q0TransferMap.put('a', q1);
		q0TransferMap.put('b', q0);
		states.add(q0);

		Map<Character, DFAState> q1TransferMap = q1.getTransferMap();
		q1TransferMap.put('a', q2);
		q1TransferMap.put('b', q3);
		states.add(q1);

		Map<Character, DFAState> q2TransferMap = q2.getTransferMap();
		q2TransferMap.put('a', q2);
		q2TransferMap.put('b', q3);
		states.add(q2);

		Map<Character, DFAState> q3TransferMap = q3.getTransferMap();
		q3TransferMap.put('a', q1);
		q3TransferMap.put('b', q0);
		states.add(q3);

		return states;
	}
}
