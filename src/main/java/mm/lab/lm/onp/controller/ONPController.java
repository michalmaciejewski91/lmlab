package mm.lab.lm.onp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import mm.lab.lm.onp.model.ONPOperationEnum;
import org.apache.log4j.Logger;

public class ONPController {

	private static final Logger LOGGER = Logger.getLogger(ONPController.class.getName());

	@FXML
	private TextField inputTextField;

	@FXML
	private Label valueLabel;

	private static final String valueLabelPrefix = "Value: ";

	private Stack stack = new Stack();

	@FXML
	private void calculate() {
		List<String> inputText = getConvertedInput();
		for (String c : inputText) {
			ONPOperationEnum operation = getOperation(c);
			if (operation == null) {
				stack.push(c);
			} else {
				Double b = Double.parseDouble(stack.pop().toString());
				Double a = Double.parseDouble(stack.pop().toString());
				stack.push(doOperation(a, b, operation));
			}
		}
		valueLabel.setText(valueLabelPrefix + stack.pop().toString());
	}

	private ArrayList<String> getConvertedInput() {
		ArrayList<String> result = new ArrayList<>();
		String inputText = inputTextField.getText();
		for (int i = 0; i < inputText.length(); i++) {
			result.add(String.valueOf(inputText.charAt(i)));
		}
		return result;
	}

	private ONPOperationEnum getOperation(String symbol) {
		switch (symbol) {
			case "+":
				return ONPOperationEnum.ADD;
			case "-":
				return ONPOperationEnum.SUB;
			case "*":
				return ONPOperationEnum.MULTI;
			case "/":
				return ONPOperationEnum.DIV;
			case "^":
				return ONPOperationEnum.EXP;
			default:
				return null;
		}
	}

	private Double doOperation(Double a, Double b, ONPOperationEnum operation) {
		Double value = null;
		String nextOperationSymbol = operation.getSymbol();
		switch (nextOperationSymbol) {
			case "+":
				value = a + b;
				break;
			case "-":
				value = a - b;
				break;
			case "*":
				value = a * b;
				break;
			case "/":
				value = a / b;
				break;
			case "^":
				value = Math.pow(a, b);
				break;
			default:
				LOGGER.error("Wrong Operation");
		}
		return value;
	}
}
