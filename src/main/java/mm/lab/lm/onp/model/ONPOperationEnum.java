package mm.lab.lm.onp.model;

public enum ONPOperationEnum {
	ADD("+"),
	SUB("-"),
	MULTI("*"),
	DIV("/"),
	EXP("^");

	private String symbol;

	ONPOperationEnum(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}
}