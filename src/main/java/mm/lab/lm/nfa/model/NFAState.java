package mm.lab.lm.nfa.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NFAState {

    public static final String VALID_STATE_COLOR = "GREEN";
    public static final String INVALID_STATE_COLOR = "RED";
    public static final String INITIAL_STATE_COLOR = "WHITE";

    private String id;
    private boolean valid;
    private Map<Character, List<NFAState>> transferMap;

    private String color;

    private NFAState() {
        this.transferMap = new HashMap<>();
        this.color = INITIAL_STATE_COLOR;

    }

    public NFAState(String id, boolean valid) {
        this();
        this.id = id;
        this.valid = valid;
        if (valid) {
            this.color = VALID_STATE_COLOR;
        } else {
            this.color = INVALID_STATE_COLOR;
        }
    }

    public String getId() {
        return id;
    }

    public boolean isValid() {
        return valid;
    }

    public Map<Character, List<NFAState>> getTransferMap() {
        return transferMap;
    }

    public String getColor() {
        return color;
    }
}
