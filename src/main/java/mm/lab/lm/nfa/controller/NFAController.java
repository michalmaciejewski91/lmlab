package mm.lab.lm.nfa.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import mm.lab.lm.nfa.model.NFAState;
import org.apache.log4j.Logger;

public class NFAController {

	private static final Logger LOGGER = Logger.getLogger(NFAController.class.getName());

	@FXML
	private TextField inputTextField;

	@FXML
	private TextField historyTextField;

	@FXML
	private Button nextStepButton;

	@FXML
	private Circle stateQ0Circle;

	@FXML
	private Circle stateQ1Circle;

	@FXML
	private Circle stateQ2Circle;

	@FXML
	private Circle stateQ3Circle;

	@FXML
	private Circle stateQ4Circle;

	@FXML
	private Circle stateQ5Circle;

	@FXML
	private Circle stateQ6Circle;

	@FXML
	private Circle stateQ7Circle;

	@FXML
	private Circle stateQ8Circle;

	@FXML
	private Circle stateQ9Circle;

	@FXML
	private Circle stateQ10Circle;

	@FXML
	private Label wrongInputLabel;

	private List<NFAState> states = initData();
	private List<NFAState> prevStates;
	private List<NFAState> currentStates;
	private String inputText;
	private int inputTextIterator = 0;

	@FXML
	private void initValidation() {
		try {
			cleanPrevValidationEffects();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		inputText = inputTextField.getText();
		if (isInputOK()) {
			currentStates = Collections.singletonList(states.get(0));
			stateQ0Circle.setFill(Paint.valueOf(states.get(0).getColor()));
			historyTextField.setText(states.get(0).getId());
			nextStepButton.setVisible(true);
		} else {
			LOGGER.debug("Input chars doesn't belong to the alphabet");
			wrongInputLabel.setVisible(true);
		}
	}

	private boolean isInputOK() {
		return inputText.matches("[ab]+");
	}

	private void cleanPrevValidationEffects() throws Exception {
		for (NFAState state : states) {
			getCircleBasedOnState(state).setFill(Paint.valueOf(NFAState.INITIAL_STATE_COLOR));
		}
		wrongInputLabel.setVisible(false);
		nextStepButton.setVisible(false);
		inputTextIterator = 0;
		historyTextField.clear();
	}

	@FXML
	private void nextStep() {
		moveOneStep();
		try {
			updateGUI(prevStates, currentStates);
			updateHistory();
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		if (inputTextIterator == inputText.length()) {
			nextStepButton.setVisible(false);
			LOGGER.info("Final state has been reached for '" + inputTextField.getText() + "'. " + isAnyStateValidAsString());
			LOGGER.info("Transition history: " + historyTextField.getText());
		}
		if (currentStates.isEmpty()) {
			nextStepButton.setVisible(false);
			LOGGER.info("No path after '" + inputText.substring(0, inputTextIterator) + "'. " + isAnyStateValidAsString());
			LOGGER.info("Transition history: " + historyTextField.getText());

		}
	}

	private void moveOneStep() {
		prevStates = currentStates;
		char nextChar = inputText.charAt(inputTextIterator);
		Set<NFAState> nextStates = new HashSet<>();
		for (NFAState state : currentStates) {
			Map<Character, List<NFAState>> transferMap = state.getTransferMap();
			if (transferMap.containsKey(nextChar)) {
				nextStates.addAll(transferMap.get(nextChar));
			}
		}
		currentStates = new ArrayList<>(nextStates);
	}

	private String isAnyStateValidAsString() {
		if (isAnyStateValid()) {
			return "ACCEPTED";
		} else {
			return "NOT ACCEPTED";
		}
	}

	private boolean isAnyStateValid() {
		List<NFAState> validStates = currentStates.stream().filter(NFAState::isValid).collect(Collectors.toList());
		return !validStates.isEmpty();
	}

	private void updateHistory() {
		StringBuilder historyBuilder = new StringBuilder(historyTextField.getText());
		historyBuilder.append(" -> ");
		for (NFAState state : currentStates) {
			historyBuilder.append(state.getId()).append(", ");
		}
		historyTextField.setText(historyBuilder.toString().replaceAll(", $", ""));
		historyTextField.positionCaret(historyTextField.getText().length());
	}

	private void updateGUI(List<NFAState> prevStates, List<NFAState> currentStates) throws Exception {
		for (NFAState state : prevStates) {
			Circle prevStateCircle = getCircleBasedOnState(state);
			prevStateCircle.setFill(Paint.valueOf(NFAState.INITIAL_STATE_COLOR));
		}
		for (NFAState state : currentStates) {
			Circle currentStateCircle = getCircleBasedOnState(state);
			currentStateCircle.setFill(Paint.valueOf(state.getColor()));
		}
		inputTextIterator++;
	}

	private Circle getCircleBasedOnState(NFAState state) throws Exception {
		String stateId = state.getId();
		if ("Q0".equals(stateId)) {
			return stateQ0Circle;
		} else if ("Q1".equals(stateId)) {
			return stateQ1Circle;
		} else if ("Q2".equals(stateId)) {
			return stateQ2Circle;
		} else if ("Q3".equals(stateId)) {
			return stateQ3Circle;
		} else if ("Q4".equals(stateId)) {
			return stateQ4Circle;
		} else if ("Q5".equals(stateId)) {
			return stateQ5Circle;
		} else if ("Q6".equals(stateId)) {
			return stateQ6Circle;
		} else if ("Q7".equals(stateId)) {
			return stateQ7Circle;
		} else if ("Q8".equals(stateId)) {
			return stateQ8Circle;
		} else if ("Q9".equals(stateId)) {
			return stateQ9Circle;
		} else if ("Q10".equals(stateId)) {
			return stateQ10Circle;
		} else {
			throw new Exception("No Circle defined for state '" + stateId + "'.");
		}
	}

	private static List<NFAState> initData() {
		List<NFAState> states = new ArrayList<>();
		states.add(new NFAState("Q0", false));
		states.add(new NFAState("Q1", true));
		states.add(new NFAState("Q2", true));
		states.add(new NFAState("Q3", true));
		states.add(new NFAState("Q4", true));
		states.add(new NFAState("Q5", true));
		states.add(new NFAState("Q6", true));
		states.add(new NFAState("Q7", true));
		states.add(new NFAState("Q8", true));
		states.add(new NFAState("Q9", true));
		states.add(new NFAState("Q10", true));

		Map<Character, List<NFAState>> q0TransferMap = states.get(0).getTransferMap();
		q0TransferMap.put('a', Collections.singletonList(states.get(1)));
		q0TransferMap.put('b', Collections.singletonList(states.get(6)));

		Map<Character, List<NFAState>> q1TransferMap = states.get(1).getTransferMap();
		q1TransferMap.put('a', Collections.singletonList(states.get(2)));
		q1TransferMap.put('b', Collections.singletonList(states.get(6)));

		Map<Character, List<NFAState>> q2TransferMap = states.get(2).getTransferMap();
		q2TransferMap.put('a', Collections.singletonList(states.get(3)));
		q2TransferMap.put('b', Collections.singletonList(states.get(6)));

		Map<Character, List<NFAState>> q3TransferMap = states.get(3).getTransferMap();
		q3TransferMap.put('a', Collections.singletonList(states.get(3)));
		q3TransferMap.put('b', Collections.singletonList(states.get(4)));

		Map<Character, List<NFAState>> q4TransferMap = states.get(4).getTransferMap();
		q4TransferMap.put('a', Collections.singletonList(states.get(9)));
		q4TransferMap.put('b', Collections.singletonList(states.get(5)));

		Map<Character, List<NFAState>> q5TransferMap = states.get(5).getTransferMap();
		q5TransferMap.put('a', Collections.singletonList(states.get(9)));

		Map<Character, List<NFAState>> q6TransferMap = states.get(6).getTransferMap();
		q6TransferMap.put('a', Collections.singletonList(states.get(1)));
		q6TransferMap.put('b', Collections.singletonList(states.get(7)));

		Map<Character, List<NFAState>> q7TransferMap = states.get(7).getTransferMap();
		q7TransferMap.put('a', Collections.singletonList(states.get(1)));
		q7TransferMap.put('b', Collections.singletonList(states.get(8)));

		Map<Character, List<NFAState>> q8TransferMap = states.get(8).getTransferMap();
		q8TransferMap.put('a', Collections.singletonList(states.get(9)));
		q8TransferMap.put('b', Collections.singletonList(states.get(8)));

		Map<Character, List<NFAState>> q9TransferMap = states.get(9).getTransferMap();
		q9TransferMap.put('a', Collections.singletonList(states.get(10)));
		q9TransferMap.put('b', Collections.singletonList(states.get(4)));

		Map<Character, List<NFAState>> q10TransferMap = states.get(10).getTransferMap();
		q10TransferMap.put('b', Collections.singletonList(states.get(4)));

		return states;
	}
}
