# LMLab
Project created for university purpose

### 1. Wykorzystując deterministyczny automat skończony (DFA) napisz program akceptujący tylko te słowa, w których przedostatnia litera jest literą 'a'. Automat ma działać nad alfabetem alfabetem {a, b}.
* Program musi wykorzystywać algorytm deterministycznego automatu skończonego, to
znaczy muszą istnieć zdefiniowane stany i przejścia między nimi.
* Język programowania dowolny - C/C++, JAVA, C# itp.
* Oprócz programu należy przedstawić schemat blokowy przedstawiający diagram
zastosowanego DFA oraz tablicę przejść w postaci wydrukowanej.
* Program w wersji z GUI, z aktualnym stanem DFA wyświetlanym po każdorazowym podaniu
litery – ocena 5, program musi umożliwić zakończenie wprowadzania znaków.
* Program w wersji konsolowej, z aktualnym stanem DFA wyświetlanym po każdorazowym
wprowadzeniu znaku – ocena 4, program musi umożliwić zakończenie wprowadzania znaków.
* Po zakończeniu etapu "wprowadzania znaków" program powinien wyświetlić aktualny stan
DFA oraz ścieżkę jaką pokonał (lista stanów). 


### 2. Wykorzystując niedeterministyczny automat skończony (NFA) napisz program akceptujący tylko te słowa, w których tylko jeden blok może mieć długość większą niż 2. Przez blok rozumiany jest maksymalny ciąg sąsiednich identycznych liter w słowie. Automat ma działać nad alfabetem alfabetem {a, b}.
* Program musi wykorzystywać algorytm niedeterministycznego automatu skończonego, to
znaczy muszą istnieć zdefiniowane stany i przejścia między nimi.
* Język programowania dowolny - C/C++, JAVA, C# itp.
* Oprócz programu należy przedstawić schemat blokowy przedstawiający diagram
zastosowanego NFA oraz tablicę przejść w postaci wydrukowanej.
* Program w wersji z GUI, z aktualnym stanem NFA wyświetlanym po każdorazowym podaniu
litery – ocena 5, program musi umożliwić zakończenie wprowadzania znaków.
* Program w wersji konsolowej, z aktualnym stanem NFA wyświetlanym po każdorazowym
wprowadzeniu znaku – ocena 4, program musi umożliwić zakończenie wprowadzania znaków.
* Po zakończeniu etapu "wprowadzania znaków" program powinien wyświetlić aktualny stan
NFA oraz ścieżkę jaką pokonał (lista stanów). 


### 3. Wykorzystując maszynę Turinga zaproponuj program dodający do siebie dwie dowolne dodatnie liczby binarne, oddzielone od siebie znakiem '#'.
### ε ε ε ε 1 0 0 1 # 1 1 1 1 1 ε ε ε ε ε ε
* Program powinien wczytywać zawartość taśmy z pliku (symbole ε można zastąpić symbolem E).
* Program powinien pokazywać położenie głowicy i aktualny stan taśmy w każdym kroku.
* Program musi wykorzystywać algorytm maszyny Turinga, to znaczy muszą istnieć zdefiniowane stany i przejścia między nimi.
* Język programowania dowolny - C/C++, JAVA, C# itp.
* Oprócz programu należy dołączyć schemat blokowy przedstawiający działanie maszyny Turinga oraz tablicę przejść.
* Program w wersji z GUI, z aktualnym stanem taśmy oraz pozycją głowicy po każdym wykonanym kroku oraz z zaznaczeniem aktualnego stanu, w którym się znajduje – ocena 5.
* Program w wersji z GUI, z aktualnym stanem taśmy oraz pozycją głowicy po każdym wykonanym kroku – ocena 4.
* Program w wersji konsolowej, z aktualnym stanem taśmy i zaznaczeniem pozycji głowicy w każdym ruchu – ocena 3.


### 4. Etap 1
Napisz program, który będzie obliczał wartość wyrażenia podanego w postaci ONP. Program ma działać na liczbach całkowitych, możliwe operacje: dodawania, odejmowanie, mnożenie, dzielenie i potęgowanie.
* Język programowania dowolny - C/C++, JAVA, C# itp.
Przykład działania programu:
* dane wejściowe: 2 5 * 1 + 2 /
* wynik działania programu: 5.5
### 4. Etap 2
Rozbuduj program napisany w poprzednim etapie o graficzny interfejs użytkownika, który będzie miał miejsce na wpisywanie wyrażenia. Program musi również posiadać przycisk, który umożliwi rozpoczęcie wykonywania obliczeń oraz okienko lub pole, w którym będzie wyświetlane wyrażenie w postaci ONP. 
##### Do zaliczenia konieczne jest wykonanie obydwóch etapów


### 5. Etap 1
Zaproponuj gramatykę, która pozwoli na wprowadzanie zdań, będących prostymi operacjami arytmetycznymi. Oznacza to, że zdania mogą zawierać liczby całkowite oraz symbole pozwalające na następujące operacje: dodawania, odejmowanie, mnożenie, dzielenie i potęgowanie. Napisz analizator składniowy, który będzie sprawdzał czy wprowadzone przykładowe zdania należą do zaproponowanej gramatyki.
* Program powinien wczytywać ciągi z pliku (poszczególne ciągi mogą być oddzielone #).
* Program powinien analizować składnię podanego zdania i generować komunikat czy badany ciąg jest poprawny.
* Język programowania dowolny - C/C++, JAVA, C# itp.
* Oprócz programu należy dołączyć wyrażenia regularne wraz z testowanymi ciągami (po 3 poprawne i 3 niepoprawne dla każdego wyrażenia).
* Program może być w wersji konsolowej.
Przykład wyrażeń akceptowanych przez gramatykę:
* 13+7
* 12+8-5*3
* 1+2^8

### 5. Etap 2
Rozbuduj program napisany w poprzednim etapie o graficzny interfejs użytkownika, który będzie miał miejsce na wpisywanie badanego zdania. Program musi również posiadać przycisk, który umożliwi rozpoczęcie analizy oraz okienko lub pole, w którym będzie wyświetlany komunikat o poprawności badanego zdania.

### 5. Etap 3
Rozbuduj gramatykę zaproponowaną w pierwszym etapie w taki sposób, aby mogła pracować na liczbach zmiennoprzecinkowych. Dodatkowo gramatyka powinna umożliwiać korzystanie z nawiasów. Po dokonaniu zmian w gramatyce należy przygotować dla niej diagram składni oraz ją zaimplementować w programie.
Przykład wyrażeń akceptowanych przez gramatykę:
* (13.5+7.4)*(-1.5-1)
* (12+8-5)*3-2*(2^4+5^(-3))/1.5
* 1.2^8
##### Do programu należy dołączyć gramatykę, diagram składni wraz z testowanymi zdaniami ( 3 poprawne i 3 niepoprawne).
